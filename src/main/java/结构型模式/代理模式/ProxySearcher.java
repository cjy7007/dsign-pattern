package 结构型模式.代理模式;

/**
 * @author cjy
 * @description: 某软件公司承接了某信息咨询公司的收费商务信息查询系统的开发任务，该系统的基本需求如下：
 * <p>
 * (1) 在进行商务信息查询之前用户需要通过身份验证，只有合法用户才能够使用该查询系统；
 * <p>
 * (2) 在进行商务信息查询时系统需要记录查询日志，以便根据查询次数收取查询费用。
 * <p>
 * 该软件公司开发人员已完成了`商务信息查询模块的开发任务`，现希望能够以一种松耦合的方式向原有系统增加身
 * 份验证和日志记录功能，客户端代码可以无区别地对待原始的商务信息查询模块和增加新功能之后的商务信息查
 * 询模块，而且可能在将来还要在该信息查询模块中增加一些新的功能。
 * <p>
 * 试使用代理模式设计并实现该收费商务信息查询系统。
 * @date: 2022$ 09/07$
 */
//代理查询类，充当代理主题角色，它是查询代理，维持了对身份验证、日志、查询对象的引用
public class ProxySearcher implements Searcher{
    //维持一个对真实主题的引用
    private RealSearcher searcher = new RealSearcher();
    private AccessValidator validator;
    private Logger logger;

    //创建访问验证对象并调用其Validate()方法实现身份验证
    public boolean validate(String userId){
        validator = new AccessValidator();
        return validator.validate(userId);
    }
    //创建日志记录对象并调用其Log()方法实现日志记录
    public void  Log(String userId){
        logger = new Logger();
        logger.Log(userId);
    }

    @Override
    public String doSearch(String userId, String keyword) {
        //如果身份验证成功，则执行查询
        if (this.validate(userId)){
            //调用真实主题对象的查询方法
            String result = searcher.DoSearch(userId, keyword);
            this.Log(userId);
            return result;
        }else {
            return null;
        }

    }


}
//身份验证类，业务类，提供Validate()来实现身份验证
class AccessValidator{
    public boolean validate(String userName){
        System.out.println("在数据库中验证用户"+userName+"是否为合法用户？");
        if (userName.equals("杨过")){
            System.out.println(userName + ": 登陆成功！");
            return true;
        }else {
            System.out.println(userName + ": 登陆失败！");
            return false;
        }
    }
}
//日志记录类，业务类
class Logger{
    //模拟实现日志记录
    public void Log(String userId){
        System.out.println("更新数据库，用户"+userId+"查询次数加一");
    }
}
//抽象查询类，充当抽象主题角色
interface Searcher{
    String doSearch(String userId, String keyword);
}
//具体查询类：充当真实主题角色，它实现查询功能
class RealSearcher{
    //模拟查询商务信息
    public String DoSearch(String userId, String keyword){
        System.out.println(String.format("用户%s使用关键字%s查询商务信息！",userId,keyword));
        return "返回具体内容";
    }
}



