package 结构型模式.外观模式;

import com.sun.deploy.util.StringUtils;

/**
 * @author cjy
 * @description: 某软件公司欲开发一个可应用于多个软件的文件加密模块，该模块
 * 可以对文件中的数据进行加密并将加密之后的数据存储在一个新文件中，具体的流程
 * 包括三个部分，分别是读取源文件、加密、保存加密之后的文件，其中，读取文件和
 * 保存文件使用流来实现，加密操作通过求模运算实现。这三个操作相对独立，为了实
 * 现代码的独立重用，让设计更符合单一职责原则，这三个操作的业务代码封装在三个
 * 不同的类中。
 * 现使用外观模式设计该文件加密模块。
 * @date: 2022$ 09/07$
 */
//加密外观类
public class EncryptFacade {
    //对使用对象的引用
    private FileReader fileReader;
    private CipherMachine cipherMachine;
    private FileWriter fileWriter;

    public EncryptFacade() {
        this.fileReader = new FileReader();
        this.cipherMachine = new CipherMachine();
        this.fileWriter = new FileWriter();
    }

    //调用其他对象的业务方法
    public void FileEncrypt(String fileNameSrc, String fileNameDes) {
        String read = fileReader.read(fileNameSrc);
        String encrypt = cipherMachine.encrypt(read);
        fileWriter.write(encrypt, fileNameDes);
    }
}

//文件读取类，充当子系统类
class FileReader {
    public String read(String fileNameSrc) {
        System.out.println("读取文件，获取明文: ");
        //通过流读取文件内容。。。。
        return "Hello World!";
    }
}

//数据加密类，充当子系统类
class CipherMachine {
    public String encrypt(String plainText) {
        System.out.println("数据加密，将明文转换为密文：");
        //取模运算加密
        return String.valueOf(plainText.hashCode());
    }
}

//文件保存类
class FileWriter {
    public void write(String encryptStr, String fileNameDes) {
        System.out.println("保存密文：" + encryptStr + ", 写入到：" + fileNameDes);
        //输出流保存
    }
}

class Client {
    public static void main(String[] args) {
        EncryptFacade encryptFacade = new EncryptFacade();
        encryptFacade.FileEncrypt("src.txt", "des.txt");
        System.out.println("----------------------------------------");
        //针对抽象外观类编程
        AbstractEncryptFacade newEncryptFacade = new NewEncryptFacade();
        newEncryptFacade.FileEncrypt("src.txt", "des.txt");
    }
}

/**
 * 在标准的外观模式中，如果需要增加、删除或者更换与外观类交互的子系统类，必须修改外观类或者客户端源代码，这将违背开闭原则，
 * 因此可以引入抽象外观类来对系统进行改进，在一定程度上可以解决该问题。
 */
class NewCipherMachine {
    public String encrypt(String plainText) {
        System.out.println("数据加密，通过位运算将明文转换为密文：");
        //位运算实现....
        return plainText.toUpperCase();
    }
}

/**
 * 如果不增加新的外观类，只能通过修改原有外观类EncryptFacade的源代码来实现加密类的更换，将原有的对CipherMachine类型对
 * 象的引用改为对NewCipherMachine类型对象的引用，这违背了开闭原则,引入一个抽象外观类，客户端针对抽象外观类编程，而在运
 * 行时再确定具体外观类。
 */
//抽象外观类
abstract class AbstractEncryptFacade {
    public abstract void FileEncrypt(String fileNameSrc, String fileNameDes);
}

//具体外观类
class NewEncryptFacade extends AbstractEncryptFacade {
    //对使用对象的引用
    private FileReader fileReader;
    private NewCipherMachine cipherMachine;  //新的加密方式
    private FileWriter fileWriter;

    public NewEncryptFacade() {
        this.fileReader = new FileReader();
        this.cipherMachine = new NewCipherMachine();
        this.fileWriter = new FileWriter();
    }

    @Override
    public void FileEncrypt(String fileNameSrc, String fileNameDes) {
        String read = fileReader.read(fileNameSrc);
        String encrypt = cipherMachine.encrypt(read);
        fileWriter.write(encrypt, fileNameDes);
    }
}


/**
 * (1) 当要为访问一系列复杂的子系统提供一个简单入口时可以使用外观模式。
 * <p>
 * (2) 客户端程序与多个子系统之间存在很大的依赖性。引入外观类可以将子系统与客户端解耦，从而提高子系统的独立性
 * 和可移植性。
 * <p>
 * (3) 在层次化结构中，可以使用外观模式定义系统中每一层的入口，层与层之间不直接产生联系，而通过外观类建立联系，
 * 降低层之间的耦合度。
 */