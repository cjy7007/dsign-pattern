package 结构型模式.适配器模式;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.util.Arrays;

/**
 * @author cjy
 * @description:适配器模式(Adapter Pattern)：将一个接口转换成客户希望的另一个接口，使接口不兼容的那些类可以一起工作，其别
 * 名为包装器(Wrapper)。适配器模式既可以作为类结构型模式，也可以作为对象结构型模式。
 * 根据适配器类与适配者类的关系不同，适配器模式可分为对象适配器和类适配器两种，在对象适配器模式中，适配器与适配者之间是关联关
 * 系；在类适配器模式中，适配器与适配者之间是继承（或实现）关系。在实际开发中，对象适配器的使用频率更高
 * <p>
 * 在对象适配器模式结构图中包含如下几个角色：
 * <p>
 * ● Target（目标抽象类）：目标抽象类定义客户所需接口，可以是一个抽象类或接口，也可以是具体类。
 * <p>
 * ● Adapter（适配器类）：适配器可以调用另一个接口，作为一个转换器，对Adaptee和Target进行适配
 * ，适配器类是适配器模式的核心，在对象适配器中，它通过继承Target并关联一个Adaptee对象使二者产生联系。
 * <p>
 * ● Adaptee（适配者类）：适配者即被适配的角色，它定义了一个已经存在的接口，这个接口需要适配，适配者类
 * 一般是一个具体类，包含了客户希望使用的业务方法，在某些情况下可能没有适配者类的源代码。
 * @date: 2022$ 09/07$
 */
//操作适配器：适配器
public class OperationAdapter implements ScoreOperation {
    //定义适配者对象
    private QuickSort sortObj;
    private BinarySearch searchObj;

    //构造适配器
    public OperationAdapter() {
        this.sortObj = new QuickSort();
        this.searchObj = new BinarySearch();
    }

    @Override
    public int[] sort(int[] array) {
        //调用适配者方法
        return sortObj.quickSort(array);
    }

    @Override
    public int search(int[] array, int key) {
        return searchObj.binarySearch(array, key);
    }
}

//抽象成绩操作类，目标接口
interface ScoreOperation {
    public int[] sort(int[] array); //成绩排序

    public int search(int[] array, int key);//成绩查找
}

//快速排序类，适配者
class QuickSort {
    public int[] quickSort(int[] array) {
        //模拟quickSort排序，此方法内容对目标不可见。
        Arrays.sort(array);
        return array;
    }
}

//二分查找，适配者
class BinarySearch {
    //模拟二分查找，此方法内容对目标不可见
    public int binarySearch(int[] array, int key) {
        return Arrays.binarySearch(array, key);
    }
}

class Client {
    public static void main(String[] args) {
        ScoreOperation operation; //针对抽象目标接口编程
        operation = new OperationAdapter();
        int[] scores = {84, 76, 50, 69, 90, 91, 88, 96};
        System.out.println("成绩排序结果: ");
        int[] result = operation.sort(scores);
        for (int i : result) {
            System.out.print(i + ",");
        }
        System.out.println();

        System.out.println("查找成绩90：");
        int isExist = operation.search(result, 90);
        if (isExist == 1) {
            System.out.println("找到成绩90");
        } else {
            System.out.println("未找到成绩90");
        }
    }
}

//补充：
/**
 * 1、除了对象适配器模式之外，适配器模式还有一种形式，那就是类适配器模式，类适配器模式和对象
 * 适配器模式最大的区别在于适配器和适配者之间的关系不同，对象适配器模式中适配器和适配者之
 * 间是关联关系，而类适配器模式中适配器和适配者是继承关系
 * <p>
 * 2、 在对象适配器的使用过程中，如果在适配器中同时包含对目标类和适配者类的引用，适配者可以通
 * 过它调用目标类中的方法，目标类也可以通过它调用适配者类中的方法，那么该适配器就是一个双向适配器
 * <p>
 * 3、缺省适配器模式(Default Adapter Pattern)：当不需要实现一个接口所提供的所有方法时，可先设
 * 计一个抽象类实现该接口，并为接口中每个方法提供一个默认实现（空方法），《那么该抽象类的子类可以选
 * 择性地覆盖父类的某些方法来实现需求》，它适用于不想使用一个接口中的所有方法的情况，又称为单接口适
 * 配器模式。
 * <p>
 * 在缺省适配器模式中，包含如下三个角色：
 * <p>
 * ● ServiceInterface（适配者接口）：它是一个接口，通常在该接口中声明了大量的方法。
 * <p>
 * ● AbstractServiceClass（缺省适配器类）：它是缺省适配器模式的核心类，使用空方法的形式实现
 * 了在ServiceInterface接口中声明的方法。通常将它定义为抽象类，因为对它进行实例化没有任何意义。
 * <p>
 * ● ConcreteServiceClass（具体业务类）：它是缺省适配器类的子类，在没有引入适配器之前，它需要
 * 实现适配者接口，因此需要实现在适配者接口中定义的所有方法，而对于一些无须使用的方法也不得不提供
 * 空实现。在有了缺省适配器之后，可以直接继承该适配器类，根据需要有选择性地覆盖在适配器类中定义的方法。
 * eg：public abstract class WindowAdapter
 * implements WindowListener, WindowStateListener, WindowFocusListener
 * <p>
 * 适配器模式将现有接口转化为客户类所期望的接口，实现了对现有类的复用，它是一种使用频率非常高的设计模式
 * ，在软件开发中得以广泛应用，在Spring等开源框架、驱动程序设计（如JDBC中的数据库驱动程序）中也使用了适
 * 配器模式
 * <p>
 * 在以下情况下可以考虑使用适配器模式：
 * <p>
 * (1) 系统需要使用一些现有的类，而这些类的接口（如方法名）不符合系统的需要，甚至没有这些类的源代码。
 * <p>
 * (2) 想创建一个可以重复使用的类，用于与一些彼此之间没有太大关联的一些类，包括一些可能在将来引进的类一起工作。
 */

