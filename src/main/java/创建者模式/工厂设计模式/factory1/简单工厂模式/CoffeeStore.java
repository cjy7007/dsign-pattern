package 创建者模式.工厂设计模式.factory1.简单工厂模式;

import 创建者模式.工厂设计模式.factory1.AmericanCoffee;
import 创建者模式.工厂设计模式.factory1.Coffee;
import 创建者模式.工厂设计模式.factory1.LatterCoffee;

/**
 * @author cjy
 * @description:
 * @date: 2022$ 07/21$
 */
public class CoffeeStore {
    public Coffee orderCoffee(String type){
        SimpleCoffeeFactory factory = new SimpleCoffeeFactory();
        //调用生产coffee的方法
        Coffee coffee = factory.createCoffee(type);
        //加配料
        coffee.addMilk();
        coffee.addSuger();
        return coffee;
    }
}
