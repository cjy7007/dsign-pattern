package 创建者模式.工厂设计模式.factory1.简单工厂模式;


import 创建者模式.工厂设计模式.factory1.Coffee;

/**
 * @author cjy
 * @description:
 * @date: 2022$ $
 */
public class Client {
    public static void main(String[] args) {
        //创建咖啡店类
        CoffeeStore coffeeStore = new CoffeeStore();
        //点咖啡
        Coffee coffee = coffeeStore.orderCoffee("latter");

        System.out.println(coffee.getName());

        //但是创建coffee还是和工厂耦合。。。。
    }
}
