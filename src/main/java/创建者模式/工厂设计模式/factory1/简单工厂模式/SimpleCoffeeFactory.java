package 创建者模式.工厂设计模式.factory1.简单工厂模式;

import 创建者模式.工厂设计模式.factory1.AmericanCoffee;
import 创建者模式.工厂设计模式.factory1.Coffee;
import 创建者模式.工厂设计模式.factory1.LatterCoffee;

/**
 * @author cjy
 * @description: 简单coffee工厂，用来生产coffee
 * @date: 2022$ 07/21$
 */
public class SimpleCoffeeFactory {
    //生产coffee
    public Coffee createCoffee(String type){
        //声明coffee类型的变量，根据不同类型创建不同的coffee子类对象
        System.out.println("工厂开始制作coffee");
        Coffee coffee = null;
        if ("american".equals(type)){
            coffee = new AmericanCoffee();
        }else if ("latter".equals(type)){
            coffee = new LatterCoffee();
        }else {
            System.out.println("对不起，您点的咖啡还没有上");
        }
        return coffee;
    }
}
