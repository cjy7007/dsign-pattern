package 创建者模式.工厂设计模式.factory1;

/**
 * @author cjy
 * @description:
 * @date: 2022$ $
 */
public class Client {
    public static void main(String[] args) {
        //创建咖啡店类
        CoffeeStore coffeeStore = new CoffeeStore();
        //点咖啡
        Coffee coffee = coffeeStore.orderCoffee("latter");

        System.out.println(coffee.getName());

        //后续要加减一种咖啡，需要改咖啡店里面的代码，违背了开闭原则
        //我们用工厂来生产对象
    }
}
