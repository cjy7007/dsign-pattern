package 创建者模式.工厂设计模式.factory1.抽象工厂模式;

import 创建者模式.工厂设计模式.factory1.Coffee;

/**
 * @author cjy
 * @description:  抽象工厂
 * @date: 2022$ $
 */
public interface DessertFactory {

    //创建甜品
    Dessert createDessert();

    //创建coffee对象的方法
    Coffee createCoffee();
}
