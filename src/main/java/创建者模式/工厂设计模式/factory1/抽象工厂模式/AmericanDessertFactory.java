package 创建者模式.工厂设计模式.factory1.抽象工厂模式;

import 创建者模式.工厂设计模式.factory1.AmericanCoffee;
import 创建者模式.工厂设计模式.factory1.Coffee;

/**
 * @author cjy
 * @description:
 * 美式风味的甜品工厂
 * 可以生产抹茶慕斯和美式咖啡
 * @date: 2022$ $
 */
public class AmericanDessertFactory implements DessertFactory{
    public Dessert createDessert() {
        return new MatchaMousse();
    }

    public Coffee createCoffee() {
        return new AmericanCoffee();
    }
}
