package 创建者模式.工厂设计模式.factory1.抽象工厂模式;


import 创建者模式.工厂设计模式.factory1.Coffee;

/**
 * @author cjy
 * @description:
 * @date: 2022$ $
 */
public class Client {
    public static void main(String[] args) {
        ItalyDessertFactory factory = new ItalyDessertFactory();

        Coffee coffee = factory.createCoffee();
        Dessert dessert = factory.createDessert();

        System.out.println(coffee.getName());
        dessert.show();
        //为了避免对象爆炸，将多个对象设计为一个产品族。能保证客户始终只使用同一个
        //产品族的对象
        //但是增加一个新产品，工厂类要进行修改
        //例如输入法换皮肤，log、颜色、字体都换了
    }
}
