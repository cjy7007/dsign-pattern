package 创建者模式.工厂设计模式.factory1.抽象工厂模式;

/**
 * @author cjy
 * @description:
 * @date: 2022$ $
 */
public class Trimisu extends Dessert {
    @Override
    public void show() {
        System.out.println("提拉米苏");
    }
}
