package 创建者模式.工厂设计模式.factory1.抽象工厂模式;

import 创建者模式.工厂设计模式.factory1.AmericanCoffee;
import 创建者模式.工厂设计模式.factory1.Coffee;
import 创建者模式.工厂设计模式.factory1.LatterCoffee;

/**
 * @author cjy
 * @description:
 * 意大利风味的甜品工厂
 * 可以生产提拉米苏和拿铁咖啡
 * @date: 2022$ $
 */
public class ItalyDessertFactory implements DessertFactory{
    public Dessert createDessert() {
        return new Trimisu();
    }

    public Coffee createCoffee() {
        return new LatterCoffee();
    }
}
