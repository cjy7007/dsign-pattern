package 创建者模式.工厂设计模式.factory1;

/**
 * @author cjy
 * @description:
 * @date: 2022$ $
 */
public class LatterCoffee extends Coffee {
    @Override
    public String getName() {
        return "拿铁咖啡";
    }
}
