package 创建者模式.工厂设计模式.factory1.工厂方法模式;

import 创建者模式.工厂设计模式.factory1.Coffee;

/**
 * @author cjy
 * @description:  抽象工厂
 * @date: 2022$ $
 */
public interface CoffeeFactory {
    //创建coffee对象的方法
    Coffee createCoffee();
}
