package 创建者模式.工厂设计模式.factory1.工厂方法模式;

import 创建者模式.工厂设计模式.factory1.AmericanCoffee;
import 创建者模式.工厂设计模式.factory1.Coffee;

/**
 * @author cjy
 * @description:
 * @date: 2022$ $
 */
public class AmericanCoffeeFactory implements CoffeeFactory {
    //美式咖啡工厂
    public Coffee createCoffee() {
        return new AmericanCoffee();
    }
}
