package 创建者模式.工厂设计模式.factory1.工厂方法模式;

import 创建者模式.工厂设计模式.factory1.AmericanCoffee;
import 创建者模式.工厂设计模式.factory1.Coffee;
import 创建者模式.工厂设计模式.factory1.LatterCoffee;

/**
 * @author cjy
 * @description:
 * @date: 2022$ $
 */
public class LatterCoffeeFactory implements CoffeeFactory {
    //拿铁咖啡工厂
    public Coffee createCoffee() {
        return new LatterCoffee();
    }
}
