package 创建者模式.工厂设计模式.factory1.工厂方法模式;


import 创建者模式.工厂设计模式.factory1.Coffee;

/**
 * @author cjy
 * @description:
 * @date: 2022$ $
 */
public class Client {
    public static void main(String[] args) {
        //创建咖啡店类
        CoffeeStore coffeeStore = new CoffeeStore();
        //创建工厂对象
        AmericanCoffeeFactory factory = new AmericanCoffeeFactory();
        coffeeStore.setFactory(factory);
        Coffee coffee = coffeeStore.orderCoffee();
        System.out.println(coffee.getName());

        //增加一个抽象的caffee工厂，每种caffee都有一个具体的工厂
        //实现了解耦，但是要很多个工厂，比较麻烦
    }
}
