package 创建者模式.工厂设计模式.factory1.工厂方法模式;

import 创建者模式.工厂设计模式.factory1.AmericanCoffee;
import 创建者模式.工厂设计模式.factory1.Coffee;
import 创建者模式.工厂设计模式.factory1.LatterCoffee;

/**
 * @author cjy
 * @description:
 * @date: 2022$ 07/21$
 */
public class CoffeeStore {

    private CoffeeFactory factory;

    public void setFactory(CoffeeFactory factory) {
        this.factory = factory;
    }

    //点咖啡功能
    public Coffee orderCoffee(){
        Coffee coffee = factory.createCoffee();

        //加配料
        coffee.addMilk();
        coffee.addSuger();
        return coffee;
    }
}
