package 创建者模式.工厂设计模式.factory1;

/**
 * @author cjy
 * @description:
 * @date: 2022$ 07/21$
 */
public abstract class Coffee {

    public abstract String getName();
    //加糖
    public void addSuger(){
        System.out.println("加糖");
    }
    //加奶
    public void addMilk(){
        System.out.println("加奶");
    }
}
