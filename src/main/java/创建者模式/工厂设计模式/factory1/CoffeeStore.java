package 创建者模式.工厂设计模式.factory1;

/**
 * @author cjy
 * @description:
 * @date: 2022$ 07/21$
 */
public class CoffeeStore {
    public Coffee orderCoffee(String type){
        //声明coffee类型的变量，根据不同类型创建不同的coffee子类对象
        Coffee coffee = null;
        if ("american".equals(type)){
            coffee = new AmericanCoffee();
        }else if ("latter".equals(type)){
            coffee = new LatterCoffee();
        }else {
            System.out.println("对不起，您点的咖啡还没有上");
        }
        //加配料
        coffee.addMilk();
        coffee.addSuger();
        return coffee;
    }
}
