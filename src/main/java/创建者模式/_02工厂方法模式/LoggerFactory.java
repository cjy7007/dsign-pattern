package 创建者模式._02工厂方法模式;

import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.io.File;

/**
 * @author cjy
 * @description:
 * Sunny软件公司欲开发一个系统运行日志记录器(Logger)，该记录器可以通过多种途径保存系统的运行日志，
 * 如通过文件记录或数据库记录，用户可以通过修改配置文件灵活地更换日志记录方式。在设计各类日志记录
 * 器时，Sunny公司的开发人员发现需要对日志记录器进行一些初始化工作，初始化参数的设置过程较为复杂，
 * 而且某些参数的设置有严格的先后次序，否则可能会发生记录失败。如何封装记录器的初始化过程并保证多
 * 种记录器切换的灵活性是Sunny公司开发人员面临的一个难题。

 * @date: 2022$ 09/05$
 */
//抽象工厂
public interface LoggerFactory {
    /*
    *  Sunny公司的开发人员通过对该需求进行分析，发现该日志记录器有两个设计要点：
       (1) 需要封装日志记录器的初始化过程，这些初始化工作较为复杂，例如需要初始
       * 化其他相关的类，还有可能需要读取配置文件（例如连接数据库或创建文件），
       * 导致代码较长，如果将它们都写在构造函数中，会导致构造函数庞大，不利于代
       * 码的修改和维护；
       (2) 用户可能需要更换日志记录方式，在客户端代码中需要提供一种灵活的方式来
       * 选择日志记录器，尽量在不修改源代码的基础上更换或者增加日志记录方式。
    *
    *
    * */
    //使用简单工厂模式可以将对象的创建和使用分离，但是违反开闭原则
    //工厂方法模式中：我们不再提供一个统一的工厂类来创建所有的产品对象，而是针对不同的产品提供不同
    //的工厂，系统提供一个与产品等级结构对应的工厂等级结构
    public Logger createLogger();
}
//日志记录器接口：抽象产品
interface Logger{
    public void writeLog();
}

//数据库日志记录器：具体产品
class DataBaseLogger implements Logger{
    @Override
    public void writeLog() {
        System.out.println("数据库日志记录！！");
    }
}

//文件日志记录器：具体产品
class FileLogger implements Logger{
    @Override
    public void writeLog() {
        System.out.println("文件日志记录！！");
    }
}

//数据库日志记录器工厂类：具体工厂
class DatabaseLoggerFactory implements LoggerFactory{
    @Override
    public Logger createLogger() {
        //连接数据库

        //创建数据库日志记录器对象
        Logger dataBaseLogger = new DataBaseLogger();

        //初始化日志记录器

        //返回日志对象
        System.out.println("创建数据库日志记录器成功！！");
        return dataBaseLogger;
    }
}

//文件日志记录器工厂类：具体工厂
class FileLoggerFactory implements LoggerFactory{
    @Override
    public Logger createLogger() {
        //创建文件日志记录器对象
        Logger fileLogger = new FileLogger();

        //初始化日志记录器

        //返回日志对象
        System.out.println("创建文件日志记录器成功！！");
        return fileLogger;
    }
}

class Client{
    public static void main(String[] args) {
        LoggerFactory factory;
        Logger logger;
        factory = new FileLoggerFactory();
        logger = factory.createLogger();
        logger.writeLog();

        //xml创建
        LoggerFactory factory1;
        Logger logger1;
        factory = (LoggerFactory) XMLUtil.getBean();
        logger = factory.createLogger();
        logger.writeLog();
    }
}

//可以在不修改任何客户端代码的基础上更换或者增加新的日志记录方式
//将具体工厂类的类名存储在xml文件中，通过读取配置文件获取类名字字符串，再通过反射机制生成对象

class XMLUtil {
    //该方法用于从XML配置文件中提取具体类类名，并返回一个实例对象
    public static Object getBean() {
        try {
            //创建DOM文档对象
            DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dFactory.newDocumentBuilder();
            Document doc;
            doc = builder.parse(new File("src/main/resources/config1.xml"));

            //获取包含类名的文本节点
            NodeList nl = doc.getElementsByTagName("className");
            Node classNode=nl.item(0).getFirstChild();
            String cName=classNode.getNodeValue();

            //通过类名生成实例对象并将其返回
            Class c=Class.forName(cName);
            Object obj=c.newInstance();
            return obj;
        }
        catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

//可以将工厂接口改为抽象类，在工厂类中直接调用日志记录器类的业务方法writeLogger();
abstract class LoggerFacory{

    public void writeLog(){
        Logger logger = this.createLogger();
        logger.writeLog();
    }

    public abstract Logger createLogger();
}

/*
1. 主要优点

        工厂方法模式的主要优点如下：

        (1) 在工厂方法模式中，工厂方法用来创建客户所需要的产品，同时还向客户隐藏了哪种具体产品类将被实例化这一细节，
        用户只需要关心所需产品对应的工厂，无须关心创建细节，甚至无须知道具体产品类的类名。

        (2) 基于工厂角色和产品角色的多态性设计是工厂方法模式的关键。它能够让工厂可以自主确定创建何种产品对象，而如
        何创建这个对象的细节则完全封装在具体工厂内部。工厂方法模式之所以又被称为多态工厂模式，就正是因为所有的具体
        工厂类都具有同一抽象父类。

        (3) 使用工厂方法模式的另一个优点是在系统中加入新产品时，无须修改抽象工厂和抽象产品提供的接口，无须修改客户
        端，也无须修改其他的具体工厂和具体产品，而只要添加一个具体工厂和具体产品就可以了，这样，系统的可扩展性也就
        变得非常好，完全符合“开闭原则”。



        2. 主要缺点

        工厂方法模式的主要缺点如下：

        (1) 在添加新产品时，需要编写新的具体产品类，而且还要提供与之对应的具体工厂类，系统中类的个数将成对增加，在
        一定程度上增加了系统的复杂度，有更多的类需要编译和运行，会给系统带来一些额外的开销。

        (2) 由于考虑到系统的可扩展性，需要引入抽象层，在客户端代码中均使用抽象层进行定义，增加了系统的抽象性和理解
        难度，且在实现时可能需要用到DOM、反射等技术，增加了系统的实现难度。

        3. 适用场景

       在以下情况下可以考虑使用工厂方法模式：

       (1) 客户端不知道它所需要的对象的类。在工厂方法模式中，客户端不需要知道具体产品类的类名，只需要知道所对应的工
       厂即可，具体的产品对象由具体工厂类创建，可将具体工厂类的类名存储在配置文件或数据库中。

       (2) 抽象工厂类通过其子类来指定创建哪个对象。在工厂方法模式中，对于抽象工厂类只需要提供一个创建产品的接口，而
       由其子类来确定具体要创建的对象，利用面向对象的多态性和里氏代换原则，在程序运行时，子类对象将覆盖父类对象，从
       而使得系统更容易扩展。
*/


