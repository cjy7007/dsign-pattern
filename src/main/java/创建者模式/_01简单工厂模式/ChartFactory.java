package 创建者模式._01简单工厂模式;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * @author cjy
 * @description: 一个生产图表的工厂ChartFactory、Chart接口充当抽象产品类，子类有3个具体产品类
 * @date: 2022$ 09/02$
 */
public class ChartFactory {
    //静态工厂方法
    public static Chart getChart(String type){
        Chart chart = null;
        if (type.equalsIgnoreCase("histogram")){
            chart = new HistogramChart();
            System.out.println("初始化柱状图！");
        }else if (type.equalsIgnoreCase("pie")){
            chart = new PieChart();
            System.out.println("初始化饼状图！");
        }else if (type.equalsIgnoreCase("line")){
            chart = new LineChart();
            System.out.println("初始化折线图！");
        }
        return chart;
    }
}

class Client{
    public static void main(String[] args) {
        //客户端测试
        Chart chart;
        chart = ChartFactory.getChart("histogram");
        chart.display();
        //如果需要更换产品，只需要修改静态工厂方法中的参数即可。

        //在创建具体的Chart对象时，每更换一个Chart对象都需要修改客户端代码中静态工厂方法的参数，
        //客户端代码将要重新编译，违反了“开闭原则”。我们可以将静态工厂方法的参数存储在XML中或者propertity
        //格式的配置文件中。
        System.out.println("----------XML创建-----------");
        String chartType = XMLUtil.getChartType();
        Chart chart1 = ChartFactory.getChart(chartType);
        chart1.display();

        //简化：将抽象产品类和工厂类合并，将静态工厂方法移到抽象产品类中

        //使用简单工厂模式设计一个可以创建不同几何形状的绘图工具，每个集合图形都具有绘制draw和
        //erase两个方法，绘制不支持的几何图形时，提示异常UnSupportedShapeException


        // 而且如果想要扩展程序，也需要对工厂类进行修改。引入新产品，就要修改工厂，违反开闭原则
        //****就需要引入工厂方法模式****

    }
}

class XMLUtil {
    public static String getChartType() {
        try {
            //java.xml,w3c.dom创建文档对象
            DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dFactory.newDocumentBuilder();
            Document doc;
            doc = documentBuilder.parse(new File("src/main/resources/config.xml"));

            //获取文本节点
            NodeList nodeList = doc.getElementsByTagName("chartType");
            Node classNode = nodeList.item(0).getFirstChild();
            String chatType = classNode.getNodeValue().trim();
            return chatType;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

//抽象图表接口
interface Chart{
    //其公有方法
    public void display();
}

//柱状图类：具体产品类
class HistogramChart implements Chart {
    public HistogramChart() {
        System.out.println("创建柱状图！！");
    }

    @Override
    public void display() {
        System.out.println("显示柱状图！！");
    }
}

//饼状图类：具体产品类
class PieChart implements Chart {
    public PieChart() {
        System.out.println("创建饼状图！！");
    }

    @Override
    public void display() {
        System.out.println("显示饼状图！！");
    }
}

//折线图类：具体产品类
class LineChart implements Chart {
    public LineChart() {
        System.out.println("创建折线图！！");
    }

    @Override
    public void display() {
        System.out.println("显示折线图！！");
    }
}
