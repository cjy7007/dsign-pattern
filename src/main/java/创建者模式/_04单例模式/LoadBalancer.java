package 创建者模式._04单例模式;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author cjy
 * @description: 有一个服务器负载均衡(Load Balance)软件的开发工作，该软件运行在一台负载
 * 均衡服务器上，可以将并发访问和数据流量分发到服务器集群中的多台设备上进行并发处理，提高系统
 * 的整体处理能力，缩短响应时间。由于集群中的服务器需要动态删减，且客户端请求需要统一分发，因
 * 此需要确保负载均衡器的唯一性，只能有一个负载均衡器来负责服务器的管理和请求的分发，否则将会
 * 带来服务器状态的不一致以及请求分配冲突等问题。如何确保负载均衡器的唯一性是该软件成功的关键。
 * @date: 2022$ 09/06$
 */
//单例类，真实情况下该类将非常复杂，包括大量初始化工作和业务方法
public class LoadBalancer {
    //私有静态成员变量，存储唯一实例
    private static LoadBalancer instance = new LoadBalancer();
    //服务器集合
    private List<String> serverList;

    //私有构造函数
    private LoadBalancer() {
        serverList = new ArrayList();
    }

    //公有静态成员方法，返回唯一实例
    public static LoadBalancer getLoadBalancer() {
        if (instance == null) {
            instance = new LoadBalancer();
        }
        return instance;
    }

    //增加服务器
    public void addServer(String server) {
        serverList.add(server);
    }

    //删除服务器
    public void removeServer(String server) {
        serverList.remove(server);
    }

    //随机获得服务器
    public String getServer() {
        Random random = new Random();
        int i = random.nextInt(serverList.size());
        return serverList.get(i);
    }
}

class Client {
    public static void main(String[] args) {
        //创建负载均衡器对象,具有唯一性
        LoadBalancer loadBalancer = LoadBalancer.getLoadBalancer();
        //增加服务器
        loadBalancer.addServer("Server 1");
        loadBalancer.addServer("Server 2");
        loadBalancer.addServer("Server 3");
        loadBalancer.addServer("Server 4");

        //模拟客户端请求分发
        for (int i = 0; i < 10; i++) {
            String server = loadBalancer.getServer();
            System.out.println("分发请求至服务器：" + server);
        }

        //IoDH
        Singleton instance1 = Singleton.getInstance();
        Singleton instance2 = Singleton.getInstance();
        System.out.println(instance1 == instance2);
    }
}

/**
 * 现在我们对负载均衡器的实现代码进行再次分析，当第一次调用getLoadBalancer()方法创建并启动负载均衡器时，instance对象为null值
 * ，因此系统将执行代码instance= new LoadBalancer()，在此过程中，由于要对LoadBalancer进行大量初始化工作，需要一段时间来创建Lo
 * adBalancer对象。而在此时，如果再一次调用getLoadBalancer()方法（通常发生在多线程环境中），由于instance尚未创建成功，仍为null
 * 值，判断条件(instance== null)为真值，因此代码instance= new LoadBalancer()将再次执行，导致最终创建了多个instance对象，这违
 * 背了单例模式的初衷，也导致系统运行发生错误。
 * <p>
 * 饿汉式：在类加载的时候就已经创建了单例对象
 * 懒汉式： 假如在某一瞬间线程A和线程B都在调用getInstance()方法，此时instance对象为null值，均能通过instance == null的判断。
 * 由于实现了synchronized加锁机制，线程A进入synchronized锁定的代码中执行实例创建代码，线程B处于排队等待状态，必须等待线程A执
 * 行完毕后才可以进入synchronized锁定代码。但当A执行完毕时，线程B并不知道实例已经创建，将继续创建新的实例，导致产生多个单例对
 * 象，违背单例模式的设计思想，因此需要进行进一步改进，在synchronized中再进行一次(instance == null)判断，这种方式称为双重检
 * 查锁定(Double-Check Locking)。
 * <p>
 * 饿汉式单例类不能实现延迟加载，不管将来用不用始终占据内存；懒汉式单例类线程安全控制烦琐，而且性能受影响。可见，无论是饿汉式单
 * 例还是懒汉式单例都存在这样那样的问题
 */

//IoDH：在单例类中增加一个静态内部类，在该内部类中创建单例对象，再将该单例对象通过getInstance()方法给外部使用

class Singleton{
    //私有构造方法
    private Singleton(){}
    //私有静态内部类，静态内部类用到时候才加载
    private static class HolderClass{
        //用到时候加载
        private final static Singleton instance = new Singleton();
    }
    //外部可访问方法
    public static Singleton getInstance(){
        return HolderClass.instance;
    }

}




