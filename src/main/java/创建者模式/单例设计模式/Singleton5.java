package 创建者模式.单例设计模式;

/**
 * @author cjy
 * @description:
 * @date: 2022$ 07/12$
 */
public class Singleton5 {

    //私有构造方法
    private Singleton5() {}

    //定义一个静态内部类
    private static class SingletonHolder{
        //在内部类中声明并初始化外部类的对象
        private static final  Singleton5 INSTANCE = new Singleton5();
    }

    //提供公共的访问方式
    public static Singleton5 getInstance(){
        return SingletonHolder.INSTANCE;
    }
}
