package 创建者模式.单例设计模式;

/**
 * @author cjy
 * @description: 懒汉式：线程安全
 * @date: 2022$ 07/12$
 */
public class Singleton3 {
    //1、私有构造方法
    private Singleton3(){}

    //声明
    private static Singleton3 instance;

    //首次使用的时候才创建
    public static synchronized Singleton3 getInstance() {

        //如果没有，则创建
        if (instance == null){
            //如果线程1等待，线程2获取到cpu的执行权，进入以后，就是创建多个对象了
            //加同步锁，没进行完不会释放
            instance = new Singleton3();
        }

        return instance;
    }
}
