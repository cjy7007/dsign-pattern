package 创建者模式.单例设计模式;

/**
 * @author cjy
 * @description: 饿汉式：静态代码块
 * @date: 2022$ 07/12$
 */
public class Singleton2 {
    //1、私有构造方法
    private Singleton2(){}

    //2、本类中创建本类对象
    private static Singleton2 instance;  //null

    //在静态代码块中进赋值
    static {
        instance = new Singleton2();
    }

    public static Singleton2 getInstance(){
        return instance;
    }
}
