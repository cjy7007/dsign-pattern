package 创建者模式.单例设计模式;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author cjy
 * @description:
 * @date: 2022$ 07/12$
 */
public class Client {
    public static void main(String[] args) throws IOException {
        //创建Singleton1对象
        Singleton1 instance1 = Singleton1.getInstance();
        Singleton1 instance2 = Singleton1.getInstance();
        //是否属于同一个对象
        System.out.println(instance1 == instance2);

        Singleton2 instance3 = Singleton2.getInstance();
        Singleton2 instance4 = Singleton2.getInstance();
        System.out.println(instance3 == instance4);

        Singleton6 instance5 = Singleton6.INSTANCE;
        Singleton6 instance6 = Singleton6.INSTANCE;
        System.out.println(instance5 == instance6);

        //序列化破坏单例模式，序列化会创建新对象

        //反射破解单例模式。

        //Runtime类使用单例模式
        Runtime runtime = Runtime.getRuntime();
        long l = runtime.freeMemory();
        System.out.println("空闲内存为：" + l);
        Process process = runtime.exec("ipconfig");
        InputStream inputStream = process.getInputStream();
        byte[] arr = new byte[1024 * 1024 * 100];
        //读取数据
        int len = inputStream.read(arr);
        System.out.println(new String(arr,0,len,"GBK"));
    }


}
