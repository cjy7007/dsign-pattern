package 创建者模式.单例设计模式;

/**
 * @author cjy
 * @description: 懒汉式：双重检查锁
 * @date: 2022$ 07/12$
 */
public class Singleton4 {
    //1、私有构造方法
    private Singleton4(){}

    //声明,指令有序
    private static volatile Singleton4 instance;

    public static Singleton4 getInstance() {
        //第一次判断,如果instance的值不为null，不需要抢占锁，直接返回对象
        if (instance == null){
            synchronized (Singleton4.class){
                //所对象后，再次判断
                if (instance == null){
                    instance = new Singleton4();
                }
            }
        }
        return instance;
    }
}
