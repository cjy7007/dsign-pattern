package 创建者模式._05原型模式;

import java.io.*;

/**
 * @author cjy
 * @description: 原型模式(Prototype Pattern)：使用原型实例指定创建对象的种类，并且通过拷贝
 * 这些原型创建新的对象。原型模式是一种对象创建型模式。需要注意的是通过克隆方法所创建的对象是全新的对象，
 * 它们在内存中拥有新的地址，通常对克隆所产生的对象进行修改对原型对象不会造成任何影响，每一个克隆对象都
 * 是相互独立的。通过不同的方式修改可以得到一系列相似但不完全相同的对象。
 * <p>
 * 通用的克隆实现方法是在具体原型类的克隆方法中实例化一个与自身类型相同的对象并将其返回，并将相关的参数
 * 传入新创建的对象中，保证它们的成员属性相同。
 * @date: 2022$ 09/06$
 */
//工作周报：具体原型类
public class WeeklyLog implements Cloneable, Serializable {

    private String name;
    private String date;
    private String content;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    //克隆方法，此处使用java的克隆机制，为浅克隆
    @Override
    public WeeklyLog clone() {
        Object obj = null;
        try {
            obj = super.clone();
            return (WeeklyLog) obj;
        } catch (CloneNotSupportedException e) {
            System.out.println("不支持克隆");
            return null;
        }
    }

    //使用序列化技术实现深克隆
    public WeeklyLog deepClone() {
        //将对象写入流中
        try {
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bao);
            oos.writeObject(this);

            //将对象从流中取出
            ByteArrayInputStream bis = new ByteArrayInputStream(bao.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bis);
            return (WeeklyLog) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("流转换异常");
            return null;
        }
    }
}

class Client {
    public static void main(String[] args) {
        WeeklyLog logPrevious = new WeeklyLog();
        logPrevious.setName("孙悟空");
        logPrevious.setDate("第12周");
        logPrevious.setContent("这周工作很忙，每天加班！");
        System.out.println("****周报****");

        System.out.println("周次：" + logPrevious.getDate());

        System.out.println("姓名：" + logPrevious.getName());

        System.out.println("内容：" + logPrevious.getContent());

        System.out.println("--------------------------------");

        WeeklyLog logNew = logPrevious.clone();
        logNew.setDate("第13周");
        System.out.println("****周报****");

        System.out.println("周次：" + logNew.getDate());

        System.out.println("姓名：" + logNew.getName());

        System.out.println("内容：" + logNew.getContent());

        System.out.println(logNew == logPrevious);
        //地址相同
        System.out.println(logNew.getContent() == logPrevious.getContent());

        //如果周报中携带了附件引用类型对象，怎么复制呢？
        /**浅克隆和深克隆的主要区别在于是否支持引用类型的成员变量的复制
         * 在浅克隆中，如果原型对象的成员变量是值类型，将复制一份给克隆
         * 对象；如果原型对象的成员变量是引用类型，则将引用对象的地址复
         * 制一份给克隆对象，也就是说原型对象和克隆对象的成员变量指向相
         * 同的内存地址。
         */
        WeeklyLog weeklyLog = logNew.deepClone();
        //地址不同
        System.out.println(weeklyLog.getContent() == logNew.getContent());
    }
}

//附件类
class Attachment {
    private String name; //附件名

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void download() {
        System.out.println("下载附件，文件名为：" + name);
    }
}

//可以引入原型管理器： 原型管理器(Prototype Manager)是将多个原型对象存储在一个集合中供客户端使用，它是一个
// 专门负责克隆对象的工厂，其中定义了一个集合用于存储原型对象，如果需要某个原型对象的一个克隆，可以通过复制集
// 合中对应的原型对象来获得。在原型管理器中针对抽象原型类进行编程，以便扩展。

