package 行为型模式.策略模式;

/**
 * @author cjy
 * @description: 为了实现打折算法的复用，并能够灵活地向系统中增加新的打折方式，Sunny软件
 * 公司开发人员使用策略模式对电影院打折方案进行重构
 * @date: 2022$ 09/21$
 */
//折扣类：抽象策略类
public interface Discount {
    public double calculate(double price);
}
//电影票类：环境类
class MovieTicket{
    private double price;
    //维持一个对抽象折扣类的引用(策略类)
    private Discount discount;

    public void setPrice(double price){
        this.price = price;
    }

    //注入一个折扣类
    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public double getPrice() {
        //调用折扣类的计算方法
        return discount.calculate(this.price);
    }
}

//学生票折扣类：具体策略类
class StudentDiscount implements Discount {
    @Override
    public double calculate(double price) {
        System.out.println("学生票");
        return price * 0.8;
    }
}

//儿童票折扣类：具体策略类
class ChildrenDiscount implements Discount{
    @Override
    public double calculate(double price) {
        System.out.println("儿童票");
        return price - 10;
    }
}

//vip会员折扣类
class VIPDiscount implements Discount{
    @Override
    public double calculate(double price) {
        System.out.println("vip票");
        System.out.println("增加积分：");
        return price * 0.5;
    }
}

class Client{
    public static void main(String[] args) {

    }
}

