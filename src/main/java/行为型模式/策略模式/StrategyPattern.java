package 行为型模式.策略模式;

/**
 * @author cjy
 * @description:
 * @date: 2022$ 09/21$
 */
public class StrategyPattern {
    /*
    * 实现某一个功能有多条途径，每一条途径对应一种算法，此时我们可以使用一种设计模式来实现灵活地选择解决途径，
    * 也能够方便地增加新的解决途径。本章我们将介绍一种为了适应算法灵活性而产生的设计模式——策略模式。
    *
    * Sunny软件公司为某电影院开发了一套影院售票系统，在该系统中需要为不同类型的用户提供不同的电影票打折方式，具体打折方案如下：

      (1) 学生凭学生证可享受票价8折优惠；

      (2) 年龄在10周岁及以下的儿童可享受每张票减免10元的优惠（原始票价需大于等于20元）；

      (3) 影院VIP用户除享受票价半价优惠外还可进行积分，积分累计到一定额度可换取电影院赠送的奖品。

      该系统在将来可能还要根据需要引入新的打折方式。
      *
    * */

    /**
     * 在策略模式中，我们可以定义一些独立的类来封装不同的算法，每一个类封装一种具体的算法，在这里，每一个封装算法的类我们都可以
     * 称之为一种策略(Strategy)，为了保证这些策略在使用时具有一致性，一般会提供一个抽象的策略类来做规则的定义，而每种算法则对
     * 应于一个具体策略类。
     * 在策略模式结构图中包含如下几个角色：
     * <p>
     * ● Context（环境类）：环境类是使用算法的角色，它在解决某个问题（即实现某个方法）时可以采用多种策略。在环境类中维持
     * 一个对抽象策略类的引用实例，用于定义所采用的策略。
     * <p>
     * ● Strategy（抽象策略类）：它为所支持的算法声明了抽象方法，是所有策略类的父类，它可以是抽象类或具体类，也可以是接
     * 口。环境类通过抽象策略类中声明的方法在运行时调用具体策略类中实现的算法。
     * <p>
     * ● ConcreteStrategy（具体策略类）：它实现了在抽象策略类中声明的算法，在运行时，具体策略类将覆盖在环境类中定义的
     * 抽象策略类对象，使用一种具体的算法实现某个业务处理。
     */
}
