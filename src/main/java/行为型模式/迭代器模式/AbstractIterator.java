package 行为型模式.迭代器模式;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cjy
 * @description:
 * @date: 2022$ 09/20$
 */
//抽象迭代器：抽象产品角色
public interface AbstractIterator {
    void next(); //移至下一个元素

    boolean isLast(); //判断是否为最后一个元素

    void previous(); //移至上一个元素

    boolean isFirst(); //判断是否为第一个元素

    Object getNextItem(); //获取下一个元素

    Object getPreviousItem(); //获取上一个元素
}

//商品迭代器：具体迭代器，具体产品角色
class ProductIterator implements AbstractIterator {
    private ProductList productList;
    private List products;
    private int cursor1; //记录正向遍历的游标
    private int cursor2; //记录逆向遍历的游标

    public ProductIterator(ProductList productList) {
        //初始化聚合类
        this.productList = productList;
        //获取元素
        this.products = productList.getObjects();
        cursor1 = 0;
        cursor2 = products.size() - 1;
    }

    @Override
    public void next() {
        if (cursor1 < products.size()) {
            cursor1++;
        }
    }

    @Override
    public boolean isLast() {
        return (cursor1 == products.size());
    }

    @Override
    public void previous() {
        if (cursor2 > -1) {
            cursor2--;
        }
    }

    @Override
    public boolean isFirst() {
        return (cursor2 == -1);
    }

    @Override
    public Object getNextItem() {
        return products.get(cursor1);
    }

    @Override
    public Object getPreviousItem() {
        return products.get(cursor2);
    }
}

//抽象聚合类：抽象工厂类
abstract class AbstractObjectList {
    protected List<Object> objects = new ArrayList<Object>();

    public AbstractObjectList(List<Object> objects) {
        this.objects = objects;
    }

    public void addObject(Object obj) {
        this.objects.add(obj);
    }

    public void removeObject(Object obj) {
        this.objects.remove(obj);
    }

    public List getObjects() {
        return this.objects;

    }

    //声明创建迭代器的抽象工厂方法
    public abstract AbstractIterator createIterator();
}

//商品数据类：具体聚合类，具体工厂类
class ProductList extends AbstractObjectList {

    public ProductList(List products) {
        super(products);
    }

    //具体工厂方法
    @Override
    public AbstractIterator createIterator() {
        return new ProductIterator(this);
    }
}

class Client {
    public static void main(String[] args) {
        List products = new ArrayList();
        products.add("倚天剑");
        products.add("屠龙刀");
        products.add("打狗棒");
        products.add("葵花宝典");
        products.add("断肠草");

        //创建聚合对象
        AbstractObjectList list = new ProductList(products);
        //创建迭代器对象
        AbstractIterator iterator = list.createIterator();

        //迭代器业务方法
        System.out.println("正向遍历：");
        while (!iterator.isLast()) {
            System.out.print(iterator.getNextItem() + ",");
            iterator.next();
        }
        System.out.println();
        System.out.println("---------------------");
        System.out.println("逆向遍历：");
        while (!iterator.isFirst()) {
            System.out.print(iterator.getPreviousItem() + ",");
            iterator.previous();
        }
    }
/**
 *  如果需要增加一个新的具体聚合类，如客户数据集合类，并且需要为客户数据集合类提供不同于商品数据集合类的
 *  正向遍历和逆向遍历操作，只需增加一个新的聚合子类和一个新的具体迭代器类即可，原有类库代码无须修改，符
 *  合“开闭原则”；
 *
 *  如果需要为ProductList类更换一个迭代器，只需要增加一个新的具体迭代器类作为抽象迭代器类的子类，重新实
 *  现遍历方法，原有迭代器代码无须修改，也符合“开闭原则”；
 *
 *  但是如果要在迭代器中增加新的方法，则需要修改抽象迭代器源代码，这将违背“开闭原则”。
 *
 */

    //使用内部类实现迭代器
    /**
     * 在迭代器模式结构图中，我们可以看到具体迭代器类和具体聚合类之间存在双重关系，其中一个关系为关联关系，在
     * 具体迭代器中需要维持一个对具体聚合对象的引用，该关联关系的目的是访问存储在聚合对象中的数据，以便迭代器
     * 能够对这些数据进行遍历操作。
     *
     * 除了使用关联关系外，为了能够让迭代器可以访问到聚合对象中的数据，我们还可以将迭代器类设计为聚合类的内部
     * 类，JDK中的迭代器类就是通过这种方法来实现的
     */
    //比如AbstractList
}

//将ProductIterator类作为ProductList类的内部类
class ProductList2 extends AbstractObjectList {


    public ProductList2(List<Object> objects) {
        super(objects);
    }

    @Override
    public AbstractIterator createIterator() {
        return null;
    }

    //商品迭代器：具体迭代器，内部类实现
    private class ProductIterator2 implements AbstractIterator {

        private int cursor1;
        private int cursor2;

        public ProductIterator2(int cursor1, int cursor2) {
            this.cursor1 = 0;
            this.cursor2 = objects.size() - 1;
        }

        @Override
        public void next() {
            if (cursor1 < objects.size()) {
                cursor1++;
            }
        }

        @Override
        public boolean isLast() {
            return (cursor1 == objects.size());
        }

        @Override
        public void previous() {
            if (cursor2 > -1) {
                cursor2--;
            }
        }

        @Override
        public boolean isFirst() {
            return (cursor2 == -1);
        }

        @Override
        public Object getNextItem() {
            return objects.get(cursor1);
        }

        @Override
        public Object getPreviousItem() {
            return objects.get(cursor2);
        }
    }
}

//jdk内置迭代器
//在Java集合框架中，常用的List和Set等聚合类都继承（或实现）了java.util.Collection接口，


/**
 * 1. 主要优点
 * <p>
 * 迭代器模式的主要优点如下：
 * <p>
 * (1) 它支持以不同的方式遍历一个聚合对象，在同一个聚合对象上可以定义多种遍历方式。在迭代器模式中只需要用一个不同的
 * 迭代器来替换原有迭代器即可改变遍历算法，我们也可以自己定义迭代器的子类以支持新的遍历方式。
 * <p>
 * (2) 迭代器简化了聚合类。由于引入了迭代器，在原有的聚合对象中不需要再自行提供数据遍历等方法，这样可以简化聚合类的
 * 设计。
 * <p>
 * (3) 在迭代器模式中，由于引入了抽象层，增加新的聚合类和迭代器类都很方便，无须修改原有代码，满足“开闭原则”的要求。
 * 2. 主要缺点
 * <p>
 * 迭代器模式的主要缺点如下：
 * <p>
 * (1) 由于迭代器模式将存储数据和遍历数据的职责分离，增加新的聚合类需要对应增加新的迭代器类，类的个数成对增加，这在
 * 一定程度上增加了系统的复杂性。
 * <p>
 * (2) 抽象迭代器的设计难度较大，需要充分考虑到系统将来的扩展，例如JDK内置迭代器Iterator就无法实现逆向遍历，如果需
 * 要实现逆向遍历，只能通过其子类ListIterator等来实现，而ListIterator迭代器无法用于操作Set类型的聚合对象。在自定义
 * 迭代器时，创建一个考虑全面的抽象迭代器并不是件很容易的事情。
 */
