package 行为型模式.观察者模式;

import 行为型模式.迭代器模式.AbstractIterator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cjy
 * @description:
 * @date: 2022$ 09/21$
 */
//抽象观察者类
public interface Observer {
    String getName();

    void setName(String name);

    //声明支援盟友的方法
    void help();

    //声明遭受攻击方法
    void beAttacked(AllyControlCenter acc);
}

//战队成员类：具体观察者类
class Player implements Observer {
    private String name;

    public Player(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    //支援盟友方法的实现
    @Override
    public void help() {
        System.out.println("坚持住！" + this.name + "来救你");
    }

    //遭受攻击方法的实现，当遭受攻击时将调用战队控制中心类的通知方法：notifyObserver()来通知盟友
    @Override
    public void beAttacked(AllyControlCenter acc) {
        System.out.println(this.name + "被攻击");
        //进行通知
        acc.notifyObserver(name);
    }
}

//战队控制中心类：抽象目标类
abstract class AllyControlCenter {
    //战队名称
    protected String allyName;
    //集合用于存储战队成员
    protected List<Observer> players = new ArrayList<Observer>();

    public String getAllyName() {
        return allyName;
    }

    public void setAllyName(String allyName) {
        this.allyName = allyName;
    }

    //注册方法
    public void join(Observer obs) {
        System.out.println(obs.getName() + "加入" + this.allyName + "战队！");
        players.add(obs);
    }

    //注销方法
    public void quit(Observer obs) {
        System.out.println(obs.getName() + "退出" + this.allyName + "战队！");
        players.remove(obs);
    }

    //声明抽象通知方法
    public abstract void notifyObserver(String name);
}

//具体战队控制中心类：具体目标类
class ConcreteAllyControlCenter extends AllyControlCenter {
    public ConcreteAllyControlCenter(String allyName) {
        System.out.println(allyName + "战队创建成功");
        System.out.println("-----------------------");
        this.allyName = allyName;
    }

    //实现通知方法
    @Override
    public void notifyObserver(String name) {
        System.out.println(this.allyName + "战队紧急通知，盟友" + name + "遭受到敌人攻击！");
        //遍历观察者，调用每一个盟友（自己除外）的支援方法
        for (Observer obs : players) {
            if (!obs.getName().equalsIgnoreCase(name)) {
                obs.help();
            }
        }
    }
}

class Client {
    public static void main(String[] args) {
        //定义观察者目标对象，战队控制中心
        ConcreteAllyControlCenter controlCenter = new ConcreteAllyControlCenter("羊村");
        //定义四个观察者对象
        Observer player1 = new Player("喜羊羊");
        controlCenter.join(player1);
        Observer player2 = new Player("美羊羊");
        controlCenter.join(player2);
        Observer player3 = new Player("懒羊羊");
        controlCenter.join(player3);
        Observer player4 = new Player("沸羊羊");
        controlCenter.join(player4);

        //某成员遭受攻击，传入战队控制中心
        player2.beAttacked(controlCenter);
    }
}

/**
 * 在本实例中，实现了两次对象之间的联动，当一个游戏玩家Player对象的beAttacked()方法被调用时，将调用AllyControlCenter
 * 的notifyObserver()方法来进行处理，而在notifyObserver()方法中又将调用其他Player对象的help()方法。
 * <p>
 * Player的beAttacked()方法、AllyControlCenter的notifyObserver()方法以及Player的help()方法构成了一个联动触发链，
 * 执行顺序如下所示：
 * Player.beAttacked() --> AllyControlCenter.notifyObserver() -->Player.help()。
 * <p>
 * 观察者模式的主要优点如下：
 * (1) 观察者模式可以实现表示层和数据逻辑层的分离，定义了稳定的消息更新传递机制，并抽象了更新接口，使得可以有各种各样不同的表示层充当具体观察者角色。
 * <p>
 * (2) 观察者模式在观察目标和观察者之间建立一个抽象的耦合。观察目标只需要维持一个抽象观察者的集合，无须了解其具体观察者。由于观察目标和观察者没有紧密地耦合在一起，因此它们可以属于不同的抽象化层次。
 * <p>
 * (3) 观察者模式支持广播通信，观察目标会向所有已注册的观察者对象发送通知，简化了一对多系统设计的难度。
 * <p>
 * (4) 观察者模式满足“开闭原则”的要求，增加新的具体观察者无须修改原有系统代码，在具体观察者与观察目标之间不存在关联关系的情况下，增加新的观察目标也很方便。
 *
 * 观察者模式的主要缺点如下：
 * <p>
 * (1) 如果一个观察目标对象有很多直接和间接观察者，将所有的观察者都通知到会花费很多时间。
 * <p>
 * (2) 如果在观察者和观察目标之间存在循环依赖，观察目标会触发它们之间进行循环调用，可能导致系统崩溃。
 * <p>
 * (3) 观察者模式没有相应的机制让观察者知道所观察的目标对象是怎么发生变化的，而仅仅只是知道观察目标发生了变化。
 */

//java.util.Observer: JDK对观察者模式的支持

