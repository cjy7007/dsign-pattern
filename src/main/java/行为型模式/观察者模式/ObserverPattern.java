package 行为型模式.观察者模式;

/**
 * @author cjy
 * @description:
 * @date: 2022$ 09/20$
 */
public class ObserverPattern {
    /**
     * 在软件系统中，有些对象之间也存在类似交通信号灯和汽车之间的关系，一个对象的状态或
     * 行为的变化将导致其他对象的状态或行为也发生改变，它们之间将产生联动，正所谓“触一
     * 而牵百发”。为了更好地描述对象之间存在的这种一对多（包括一对一）的联动，观察者模式
     * 应运而生，它定义了对象之间一种一对多的依赖关系，让一个对象的改变能够影响其他对象。
     * 本章我们将学习用于实现对象间联动的观察者模式。
     * <p>
     * Sunny软件公司欲开发一款多人联机对战游戏（类似魔兽世界、星际争霸等游戏），在该游戏中，多
     * 个玩家可以加入同一战队组成联盟，当战队中某一成员受到敌人攻击时将给所有其他盟友发送通知，
     * 盟友收到通知后将作出响应。
     * <p>
     * Sunny软件公司开发人员需要提供一个设计方案来实现战队成员之间的联动。
     *
     * 在观察者模式中，发生改变的对象称为观察目标，而被通知的对象称为观察者，一个观察目标可以对
     * 应多个观察者，而且这些观察者之间可以没有任何相互联系，可以根据需要增加和删除观察者，使得
     * 系统更易于扩展。
     *
     * 观察者模式的别名包括发布-订阅（Publish/Subscribe）模式、模型-视图（Model/View）模式、
     * 源-监听器（Source/Listener）模式或从属者（Dependents）模式
     */

    /**
     * 在观察者模式结构图中包含如下几个角色：
     * <p>
     * ● Subject（目标）：目标又称为主题，它是指被观察的对象。在目标中定义了一个观察者集合，一个观察目标
     * 可以接受任意数量的观察者来观察，它提供一系列方法来增加和删除观察者对象，同时它定义了通知方法notify()
     * 。目标类可以是接口，也可以是抽象类或具体类。
     * <p>
     * ● ConcreteSubject（具体目标）：具体目标是目标类的子类，通常它包含有经常发生改变的数据，当它的状态发
     * 生改变时，向它的各个观察者发出通知；同时它还实现了在目标类中定义的抽象业务逻辑方法（如果有的话）。如果
     * 无须扩展目标类，则具体目标类可以省略。
     * <p>
     * ● Observer（观察者）：观察者将对观察目标的改变做出反应，观察者一般定义为接口，该接口声明了更新数据的
     * 方法update()，因此又称为抽象观察者。
     * <p>
     * ● ConcreteObserver（具体观察者）：在具体观察者中维护一个指向具体目标对象的引用，它存储具体观察者的有
     * 关状态，这些状态需要和具体目标的状态保持一致；它实现了在抽象观察者Observer中定义的update()方法。通常
     * 在实现时，可以调用具体目标类的attach()方法将自己添加到目标类的集合中或通过detach()方法将自己从目标类
     * 的集合中删除。
     */
}
