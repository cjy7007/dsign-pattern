package 行为型模式.命令模式;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cjy
 * @description: Sunny软件公司开发人员为公司内部OA系统开发了一个桌面版应用程序，该应用程序为用户提供了一系列自定义功能键，
 * 用户可以通过这些功能键来实现一些快捷操作。Sunny软件公司开发人员通过分析，发现不同的用户可能会有不同的使用习惯，在设置功
 * 能键的时候每个人都有自己的喜好，例如有的人喜欢将第一个功能键设置为“打开帮助文档”，有的人则喜欢将该功能键设置为“最小化至
 * 托盘”，为了让用户能够灵活地进行功能键的设置，开发人员提供了一个“功能键设置”窗口
 * <p>
 * 在软件开发中，我们经常需要向某些对象发送请求（调用其中的某个或某些方法），但是并不知道请求的接收者是谁，也不知道被请求
 * 的操作是哪个，此时，我们特别希望能够以一种松耦合的方式来设计软件，使得请求发送者与请求接收者能够消除彼此之间的耦合，让
 * 对象之间的调用关系更加灵活
 * <p>
 * 在命令模式结构图中包含如下几个角色：
 * <p>
 * ● Command（抽象命令类）：抽象命令类一般是一个抽象类或接口，在其中声明了用于执行请求的execute()等方法，通过这些
 * 方法可以调用请求接收者的相关操作。
 * <p>
 * ● ConcreteCommand（具体命令类）：具体命令类是抽象命令类的子类，实现了在抽象命令类中声明的方法，它对应具体的接收
 * 者对象，将接收者对象的动作绑定其中。在实现execute()方法时，将调用接收者对象的相关操作(Action)。
 * <p>
 * ● Invoker（调用者）：调用者即请求发送者，它通过命令对象来执行请求。一个调用者并不需要在设计时确定其接收者，因此
 * 它只与抽象命令类之间存在关联关系。在程序运行时可以将一个具体命令对象注入其中，再调用具体命令对象的execute()方法，
 * 从而实现间接调用请求接收者的相关操作。
 * <p>
 * ● Receiver（接收者）：接收者执行与请求相关的操作，它具体实现对请求的业务处理。
 * @date: 2022$ 09/09$
 */

//功能键设置窗口类
class SettingWindow {
    //窗口标题
    private String title;
    //定义一个List来存储所有功能键
    private List<FunctionButton> functionButtons = new ArrayList<FunctionButton>();

    //构造方法，传入窗口标题
    public SettingWindow(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void addFunctionButton(FunctionButton fb) {
        functionButtons.add(fb);
    }

    public void removeFunctionButton(FunctionButton fb) {
        functionButtons.remove(fb);
    }

    //显示窗口和功能键
    public void display() {
        System.out.println("显示窗口：" + this.title);
        System.out.println("显示功能键：");
        for (FunctionButton fb : functionButtons) {
            System.out.println(fb.getName());
        }
        System.out.println("-----------------------");
    }
}

//功能键类：请求发送者/调用者
class FunctionButton {
    //功能键名称
    private String name;
    //抽象命令对象
    private Command command;

    public FunctionButton(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    //为功能键注入命令
    public void setCommand(Command command) {
        this.command = command;
    }

    //发送请求的方法
    public void onClick() {
        System.out.println("点击功能键：" + name);
        //执行接收者的方法
        command.execute();
    }
}

//抽象命令类
public interface Command {
    public void execute();
}

//帮助命令类：具体命令类
class HelpCommand implements Command {
    //声明帮助处理类对象
    private HelpHandler helpHandler;

    public HelpCommand() {
        helpHandler = new HelpHandler();
    }

    //命令执行方法
    @Override
    public void execute() {
        helpHandler.display();
    }
}

//最小化命令类：具体命令类
class MinimizeCommand implements Command {
    //引用窗口最小化对象
    private WindowHandler windowHandler;

    //构造
    public MinimizeCommand() {
        windowHandler = new WindowHandler();
    }

    //命令执行方法，将调用请求接收者的业务方法
    @Override
    public void execute() {
        windowHandler.minimize();
    }
}

//帮助文档处理类，请求的接收者
class HelpHandler {
    public void display() {
        System.out.println("显示帮助文档！");
    }
}

//窗口处理类，请求的接收者
class WindowHandler {
    public void minimize() {
        System.out.println("将窗口最小化至托盘！");
    }
}

class Client {
    public static void main(String[] args) {
        SettingWindow sw = new SettingWindow("功能键设置窗口");
        FunctionButton fb1 = new FunctionButton("功能键1");
        FunctionButton fb2 = new FunctionButton("功能键2");

        //生成具体的命令对象
        Command command1 = new HelpCommand();
        Command command2 = new MinimizeCommand();

        //将命令对象注入功能键
        fb1.setCommand(command1);
        fb2.setCommand(command2);

        //窗口增加功能键
        sw.addFunctionButton(fb1);
        sw.addFunctionButton(fb2);
        sw.display();

        //调用功能键的业务方法
        fb1.onClick();
        fb2.onClick();
    }
}

/**
 * 如果需要修改功能键的功能，例如某个功能键可以实现“自动截屏”,只需要对应增加一个新的具体命令类，在该命令类AutoScreeCommand
 * 与屏幕处理者(ScreenHandler)之间创建一个关联关系, 然后将该具体命令类的对象通过配置文件注入到某个功能键即可，原有代码无须
 * 修改，符合“开闭原则”。在此过程中，每一个具体命令类对应一个请求的处理者（接收者），通过向请求发送者注入不同的具体命令对象
 * 可以使得相同的发送者对应不同的接收者，从而实现“将一个请求封装为一个对象，用不同的请求对客户进行参数化”，客户端只需要将具.
 * 体命令对象作为参数注入请求发送者，无须直接操作请求的接收者。
 */

//命令队列的实现，有时候我们需要将多个请求排队，当一个请求发送者发送一个请求时，将不只一个请求接收者产生响应，这些请求接收者
//将逐个执行业务方法，完成对请求的处理。可以使用命令队列来实现。

class CommandQueue {
    //定义一个ArrayList来存储命令队列
    private List<Command> commands = new ArrayList<Command>();

    public void addCommand(Command command) {
        commands.add(command);
    }

    public void removeCommand(Command command) {
        commands.remove(command);
    }

    //循环调用每个命令对象的execute()方法
    public void execute(){
        for (Command command : commands) {
            command.execute();
        }
    }
}

//请求发送者将对CommandQueue编程，
class Invoker{
    private CommandQueue commandQueue;

    //构造注入
    public Invoker(CommandQueue commandQueue){
        this.commandQueue = commandQueue;
    }

    //调用execute方法

    public void call(){
        commandQueue.execute();
    }
}

/*如果请求接收者的接收次序没有严格的先后次序，我们还可以使用多线程技术来
并发调用命令对象的execute()方法，从而提高程序的执行效率。*/

