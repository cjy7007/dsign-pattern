package 面向对象设计原则.迪米特法则;

/**
 * @version v1.0
 * @ClassName: Company
 * @Description: 媒体公司类
 * @Author: cjy
 */
public class Company {
    private String name;

    public String getName() {
        return name;
    }

    public Company(String name) {
        this.name = name;
    }
}
