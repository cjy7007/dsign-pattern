package 面向对象设计原则.迪米特法则;

/**
 * @version v1.0
 * @ClassName: Star
 * @Description: 明星类
 * @Author: cjy
 */
public class Star {
    private String name;

    public Star(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
