package 面向对象设计原则.迪米特法则;

/**
 * @version v1.0
 * @ClassName: Fans
 * @Description: 粉丝类
 * @Author: cjy
 */
public class Fans {

    private String name;

    public String getName() {
        return name;
    }

    public Fans(String name) {
        this.name = name;
    }
}
