package 面向对象设计原则.依赖倒转原则;

/**
 * @author cjy
 * @description:
 * @date: 2022$ 07/11$
 */
public interface Memory {
    public void save();
}
