package 面向对象设计原则.依赖倒转原则;

/**
 * @author cjy
 * @description:
 * @date: 2022$ 07/11$
 */
public class TestComputer {
    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.setCpu(new IntelCpu());
        computer.setMemory(new KingstonMemory());
        computer.setHardDisk(new XiJieHardDisk());

        computer.run();
    }
}
