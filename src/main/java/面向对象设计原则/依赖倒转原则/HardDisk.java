package 面向对象设计原则.依赖倒转原则;

/**
 * @author cjy
 * @description:
 * @date: 2022$ 07/11$
 */
public interface HardDisk {
    //硬盘保存数据
    void save(String data);

    //硬盘取数据
    String get();
}
