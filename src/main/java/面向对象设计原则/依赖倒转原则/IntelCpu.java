package 面向对象设计原则.依赖倒转原则;

/**
 * @author cjy
 * @description:
 * @date: 2022$ 07/11$
 */
public class IntelCpu implements Cpu {

    @Override
    public void save() {
        System.out.println("使用Intel处理器");
    }
}
