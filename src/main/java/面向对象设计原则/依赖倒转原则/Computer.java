package 面向对象设计原则.依赖倒转原则;

/**
 * @author cjy
 * @description:
 * @date: 2022$ 07/11$
 */
public class Computer {

    //高层（计算机）不应该依赖底层（硬件），而是共同依赖其抽象（接口）
    //细节应该依赖抽象
    //这就让计算机可以方便更换硬件，因为它的依赖是接口。
    private HardDisk hardDisk;
    private Cpu cpu;
    private Memory memory;

    public HardDisk getHardDisk() {
        return hardDisk;
    }

    public void setHardDisk(HardDisk hardDisk) {
        this.hardDisk = hardDisk;
    }

    public Cpu getCpu() {
        return cpu;
    }

    public void setCpu(Cpu cpu) {
        this.cpu = cpu;
    }

    public Memory getMemory() {
        return memory;
    }

    public void setMemory(Memory memory) {
        this.memory = memory;
    }

    public void run(){
        System.out.println("计算机工作");
        cpu.save();
        memory.save();
        String data = hardDisk.get();
        System.out.println("从硬盘中获取的数据为：" + data);
    }
}
