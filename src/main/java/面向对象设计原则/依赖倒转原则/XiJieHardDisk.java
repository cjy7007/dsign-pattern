package 面向对象设计原则.依赖倒转原则;

/**
 * @author cjy
 * @description:
 * @date: 2022$ 07/11$
 */
public class XiJieHardDisk implements HardDisk {

     @Override
     public void save(String data) {
        System.out.println("使用希捷硬盘存储数据" + data);
    }

     @Override
     public String get() {
        System.out.println("使用希捷硬盘取数据");
        return "数据";
    }
}
